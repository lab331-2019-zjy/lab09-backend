package se331.lab.rest.services;

import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudent();
    Student findById(Long id);
    Student saveStudent(Student student);


}
