package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.StudentRepository;


@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        studentRepository.save(Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Junyu ")
                .surname("Zhou")
                .gpa(4.0).image("https://www.mobafire.com/images/avatars/teemo-classic.png")
                .penAmount(100)
                .description("Came from db!")
                .build());
        studentRepository.save(Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("Zhou ")
                .surname("Junyu")
                .gpa(4.00)
                .image("https://www.mobafire.com/images/avatars/teemo-classic.png")
                .penAmount(2)
                .description("Came from db!")
                .build());


    }
}
