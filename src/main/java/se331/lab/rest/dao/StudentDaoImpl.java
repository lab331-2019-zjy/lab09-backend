package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Profile("LabDao")
@Repository
public class StudentDaoImpl implements StudentDao{
    List<Student> students;
    public StudentDaoImpl(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Junyu ")
                .surname("Zhou")
                .gpa(100.0)
                .image("https://www.mobafire.com/images/avatars/teemo-classic.png")
                .penAmount(15)
                .description("Cool")
                .build());
        this.students.add(Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("Zhou ")
                .surname("Junyu")
                .gpa(4.00)
                .image("https://www.mobafire.com/images/avatars/teemo-classic.png")
                .penAmount(2)
                .description("666")
                .build());


    }

    @Override
    public List<Student> getAllStudent() {
        log.info("Lab dao is called");
        return students;
    }

    @Override
    public Student findById(Long id) {
        return students.get((int) (id -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }

}
